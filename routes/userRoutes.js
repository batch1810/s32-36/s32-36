const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController")



router.post("/details", (req, res)=>{
	userController.getProfile(req.body.id).then(resultFromController=>res.send(resultFromController))
})

module.exports = router;